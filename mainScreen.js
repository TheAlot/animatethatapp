import {View, StatusBar} from 'react-native';
import React, {useContext} from 'react';
import {StackChangeContext} from './App';
import Text from './customText';
import Button from './customButton';

export default function MainScreen(props) {
  const goToStack = useContext(StackChangeContext);
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 28}}>This is the main screen</Text>
      <Button
        title="Go to sign in "
        onPress={() => {
          //ref.current.animateNextTransition();
          goToStack('Auth');
        }}
      />
    </View>
  );
}
