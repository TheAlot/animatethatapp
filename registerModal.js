import React, {useState, createContext} from 'react';
import {Platform} from 'react-native';
import Modal from 'react-native-modal';
import * as screens from './screens';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

const Stack = createStackNavigator();
export const ModalContext = createContext();

export default function RegisterModal(props) {
  return (
    <Modal
      isVisible={props.isVisible}
      useNativeDriver={true}
      backdropOpacity={1}
      backdropColor="white"
      style={{padding: 0, margin: 0}}
      hasBackdrop={false}>
      <ModalContext.Provider
        value={{
          toggleModalVisibility: props.toggleModalVisibility,
          navigateToApp: props.navigateToApp,
        }}>
        <Stack.Navigator
          screenOptions={{
            headerStatusBarHeight: 0,
            headerTitleStyle: {
              fontFamily: Platform.OS === 'ios' ? 'Arial' : 'Roboto',
            },
            initialRouteName: 'ScreenOne',
            ...TransitionPresets.SlideFromRightIOS,
          }}>
          <Stack.Screen name="ScreenOne" component={screens.ScreenOne} />
          <Stack.Screen name="ScreenTwo" component={screens.ScreenTwo} />
          <Stack.Screen name="ScreenThree" component={screens.ScreenThree} />
        </Stack.Navigator>
      </ModalContext.Provider>
    </Modal>
  );
}
