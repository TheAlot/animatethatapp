import React from 'react';
import {Button} from 'react-native-elements';
import {StyleSheet, Platform} from 'react-native';

export default function CustomButton(props) {
  return (
    <Button {...props} titleStyle={[styles.text, props.titleStyle]}>
      {props.children}
    </Button>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: Platform.OS === 'ios' ? 'Arial' : 'Roboto',
  },
});
