import React, {useState, useRef, useContext} from 'react';
import {View, StatusBar} from 'react-native';
import RegisterModal from './registerModal';
import {StackChangeContext} from './App';
import Text from './customText';
import Button from './customButton';

export default function SignInScreen(props) {
  const [registerModalOpen, setRegisterModalOpen] = useState(false);
  const goToStack = useContext(StackChangeContext);

  const toggleModalOpen = () => {
    setRegisterModalOpen(!registerModalOpen);
  };
  const navigateToApp = () => {
    //ref.current.animateNextTransition();
    goToStack('App');
  };
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <StatusBar translucent />
      <Text style={{fontSize: 24}} >Sign in</Text>
      <Button title={'go to app'} onPress={navigateToApp} />
      <View style={{height: 20}} />
      <Button title={'open modal '} onPress={toggleModalOpen} />
      <RegisterModal
        isVisible={registerModalOpen}
        toggleModalVisibility={toggleModalOpen}
        navigateToApp={navigateToApp}
      />
    </View>
  );
}
