import React, {useContext} from 'react';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import Button from 'react-native-really-awesome-button/src/themes/bojack';
import {View, StatusBar, Text, StyleSheet, Platform} from 'react-native';

import {StackChangeContext} from './App';

export default function CustomDrawerContent(props) {
  const goToStack = useContext(StackChangeContext);

  return (
    <View style={{flex: 1}}>
      <StatusBar translucent={true} />
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} labelStyle={styles.text} />
      </DrawerContentScrollView>
      <Button
        textFontFamily="Oswald-Bold"
        stretch={true}
        raiseLevel={0}
        borderRadius={0}
        onPress={() => {
          goToStack('Auth');
        }}>
        Go to Login
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: Platform.OS === 'ios' ? 'Arial' : 'Roboto',
  },
});
