import {View, Text, Button} from 'react-native';
import React, {useContext} from 'react';
import {StackChangeContext} from './App';

export default function AboutScreen(props) {
  const context = useContext(StackChangeContext);
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>About</Text>
    </View>
  );
}
