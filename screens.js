import React, {useContext} from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {ModalContext} from './registerModal';

export const ScreenOne = function ScreenOne({route, navigation}) {
  const context = useContext(ModalContext);
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text>First screen</Text>
        <Button
          title="Close modal"
          onPress={() => {
            context.toggleModalVisibility();
          }}
        />
      </View>
      <View style={styles.contentContainer}>
        <Text>This is the content 1</Text>
      </View>
      <View style={styles.bottomContainer}>
        <Button
          title="Next"
          style={styles.button}
          onPress={() => {
            navigation.push('ScreenTwo');
          }}
        />
      </View>
    </View>
  );
};

export const ScreenTwo = function ScreenTwo({route, navigation}) {
  const context = useContext(ModalContext);
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text>Second screen</Text>
        <Button
          title="Close modal"
          onPress={() => {
            context.toggleModalVisibility();
          }}
        />
      </View>
      <View style={styles.contentContainer}>
        <Text>This is the content 2</Text>
      </View>
      <View style={styles.bottomContainer}>
        <Button
          title="Next"
          style={styles.button}
          onPress={() => {
            navigation.push('ScreenThree');
          }}
        />
      </View>
    </View>
  );
};

export const ScreenThree = function ScreenThree({route}) {
  const context = useContext(ModalContext);
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text>Third screen</Text>
        <Button
          title="Close modal"
          onPress={() => {
            context.toggleModalVisibility();
          }}
        />
      </View>
      <View style={styles.contentContainer}>
        <Text>This is the content 3</Text>
      </View>
      <View style={styles.bottomContainer}>
        <Button
          title="Next"
          style={styles.button}
          onPress={() => {
            context.toggleModalVisibility();
            context.navigateToApp();
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  titleContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: 16,
  },
  contentContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column-reverse',
    marginBottom: 16,
  },
  button: {
    width: 50,
  },
});
