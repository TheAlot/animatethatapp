import * as React from 'react';
import {
  Text,
  SafeAreaView,
  View,
  StyleSheet,
  Button,
  UIManager,
} from 'react-native';

import { Transition, Transitioning } from 'react-native-reanimated';

const COLORS = [
  'cyan',
  'magenta',
  'yellow',
  'blue',
  'brown',
  'plum',
  'aquamarine',
];

function insertColorAtRandom(array) {
  const usedColors = new Set(array);
  const color = COLORS.find(color => !usedColors.has(color));
  if (color) {
    const index = Math.floor(Math.random() * (array.length + 1));
    const result = [...array];
    result.splice(index, 0, color);
    return result;
  } else {
    return array;
  }
}

function deleteRandomColor(array) {
  if (array.length > 0) {
    const index = Math.floor(Math.random() * array.length);
    const result = [...array];
    result.splice(index, 1);
    return result;
  } else {
    return array;
  }
}

class App extends React.Component {
  state = {
    items: ['aquamarine'],
  };
  insertAtRandom = () => {
    springTransition();
    this.setState({
      items: insertColorAtRandom(this.state.items),
    });
  };
  deleteRandom = () => {
    springTransition();
    this.setState({
      items: deleteRandomColor(this.state.items),
    });
  };
  render() {
    const items = this.state.items.map(item => (
      <View key={item} style={[styles.item, { backgroundColor: item }]} />
    ));
    return (
      <SafeAreaView style={styles.container}>
        {items}
        <View style={styles.spacer} />
        <Button title="Add item" onPress={this.insertAtRandom} />
        <Button title="Delete item" onPress={this.deleteRandom} />
      </SafeAreaView>
    );
  }
}

const transition = (
  <Transition.Together>
    <Transition.In type="fade" />
    <Transition.Out type="fade" />
    <Transition.Change interpolation="easeInOut" />
  </Transition.Together>
);

const ROOT_REF = React.createRef();

function springTransition() {
  ROOT_REF.current.animateNextTransition();
}

export default function AppContainer() {
  return (
    <Transitioning.View
      style={styles.root}
      transition={transition}
      ref={ROOT_REF}>
      <App />
    </Transitioning.View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  spacer: {
    flex: 1,
  },
  item: {
    height: 30,
    marginVertical: 2,
    marginHorizontal: 10,
  },
});
