import React, {useState, createContext} from 'react';
import 'react-native-gesture-handler';
import SignInScreen from './signIn';
import MainScreen from './mainScreen';
import AboutScreen from './about';
import {StatusBar, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import CustomDrawerContent from './customDrawerContent';

export const StackChangeContext = createContext(null);

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  const [currentStack, setCurrentStack] = useState('App');

  const renderContent = () => {
    return (
      <View style={{flex: 1}}>
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle="dark-content"
        />
        <Stack.Navigator headerMode="none">
          {currentStack === 'Auth' ? (
            <Stack.Screen
              name="Auth"
              component={SignInScreen}
              options={{
                headerShown: false,
              }}
            />
          ) : (
            <Stack.Screen name="AppDrawer" component={DrawerNavigator} />
          )}
        </Stack.Navigator>
      </View>
    );
  };

  return (
    <StackChangeContext.Provider
      value={newStack => {
        setCurrentStack(newStack);
      }}>
      <NavigationContainer>{renderContent()}</NavigationContainer>
    </StackChangeContext.Provider>
  );
}

function DrawerNavigator() {
  return (
    <View style={{flex: 1}}>
      <Drawer.Navigator
        drawerType="slide"
        drawerContent={props => <CustomDrawerContent {...props} />}>
        <Drawer.Screen name="App" component={MainScreen} />
        <Drawer.Screen name="About" component={AboutScreen} />
      </Drawer.Navigator>
    </View>
  );
}
